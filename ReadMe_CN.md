# flux架构，是我理解错了吗？

> 笔者原意是希望能通过阅读redux文档和其源码彻底掌握其设计思想和使用技巧。一开始一头雾水不清楚为什么这样设计，最后还是一头雾水不清楚为什么这样设计？

## 一、flux的基本概念

以下内容引用自(阮一峰老师的博客文章)[http://www.ruanyifeng.com/blog/2016/01/flux.html]

首先，Flux将一个应用分成四个部分。

> - **View**： 视图层
> - **Action**（动作）：视图层发出的消息（比如mouseClick）
> - **Dispatcher**（派发器）：用来接收Actions、执行回调函数
> - **Store**（数据层）：用来存放应用的状态，一旦发生变动，就提醒Views要更新页面

![img](http://www.ruanyifeng.com/blogimg/asset/2016/bg2016011503.png)

Flux 的最大特点，就是数据的"单向流动"。

> 1. 用户访问 View
> 2. View 发出用户的 Action
> 3. Dispatcher 收到 Action，要求 Store 进行相应的更新
> 4. Store 更新后，发出一个"change"事件
> 5. View 收到"change"事件后，更新页面

上面过程中，数据总是"单向流动"，任何相邻的部分都不会发生数据的"双向流动"。这保证了流程的清晰。

## 二、对flux的反思

单向数据流既然是这里的核心，那不禁要问，实现单向数据流的关键点在于什么？**Dispatcher！！**统一的分发器能够使得控制数据的流动！！

```
Q：但是，dispatcher是什么？
A：统一的分发器。
Q：分发啥？
A：分发Actions事件。
Q：Actions事件是什么？
A：视图层发出的消息。
Q：但是我那么多组件，有那么多Actions，命名冲突了怎么办？我怎么知道那个组件对应哪几个Actions？编写Actions 时候怎么知道对应数据库结构？
A：你怎么那么多问题！
```
flux 架构统一了将多层级Components 投射到扁平的 Store结构中。用户需要预定义Store的结构，使得初始化Flux的时候就完整知道Store的结构。

但是这样好吗？笔者觉得这样丧失了扩展性，有时候组件的临时变量丢进去也会常驻在对象中。Redux 的 Dispatcher 实现直接是用大switch结构组合而成，需要每次迭代判断触发Actions。为什么要这样做呢？

### 疑问如下：

1. 为什么非要用一个巨大的Store存起来？
2. Actions真的是View的附属品吗？
3. 将树状的Components树投射到没有明显层级的Stroe真的合理吗？

**我的回答是：**

1. 并非必要，不同的Component可以有不用的Stroe

2. Actions 不仅仅是View的附属品，其中还可以进一步划分

3. 不合理，允许并设计不同层级的可组合性是必要的。

> 吐槽：Redux用函数编写的结构真的很难受，它的d.ts文件简直就是需要那么复杂吗！

## 三、个人对flux架构的理解

### Store：

```
1.作为最底层数据层DataBase，应有CURD接口或者命令。
2.中间层是Actions,对应的数据库概念应该是“事务”。就是该“Actions"事务需要修改DataBase的什么数据，如何处理修改的异常等问题。
3.Dispatcher实现，是提供Action的名称“Actionxxx”,调用参数，来统一接口调用Actions。
4.产生新Store通过继承对应的抽象类（本质是原型模式）
5.父子关系的建立，需要在Store的构造函数中调用内置方法。
6.父子关系的解绑同上。
```

### Dispatcher：

```
必然依附于Store，
有同一的实现，可重写，
里面内置中间件调用接口的实现以及Store事件的调用接口的实现。
```

### View：

```
树级组件，
局部Store按需创建，按需建立父子连接。
View层的事件Event触发 = 应同时调用对应的Actions，支持异步函数。
```

### Actions：

```
割离为
View的Events 与 Stores的Actions
```

![我自定义的flux架构](https://oscimg.oschina.net/oscnet/1289b1a18e6b3dd6df24e2915d30b8a074c.jpg "我自定义的flux架构")

这是我自定义的flux架构。

若产生Store的父子层级，则dispatcher的最后会溯源执行EventPool。

可以继承Store的类以重载Actions，Dispatcher




