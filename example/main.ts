import * as data from "../src";
function sleep(d:number){
    for(var t = Date.now();Date.now() - t <= d;);
  }

export class Store extends data.DataBase{
    _name:string = "Store";
    public Add(actionType:data.actionTypes){
        this.state["type"] = actionType["args"]
    }
}
export const Global = new Store();
Global.subscribe(function(){
    console.log(Global.getState())
})
export class Model extends data.DataBase{
    _name:string = "Model";

    constructor(){
        super();
        // this._name = ... //动态_name,需要配合构造函数实现
        this.attach(Global) //赋值父类
    }
    public Add(actionType:data.actionTypes):data.stateType{
        let newState = Object.assign({},this.state)
        newState["type"] = actionType["args"]
        console.log("=======Add======")
        return newState
    }
    public async NULL(actionType:data.actionTypes):Promise<data.stateType>{
        let newState = Object.assign({},this.state)
        newState["type"] = undefined;
        sleep(3000)
        console.log("======NULL=======")
        return newState
    }
}
export const Local = new Model();
Local.subscribe(async function(prevState:any,NewState:any,actionType:data.actionTypes){
    console.log(prevState,NewState,actionType)
    sleep(5000)
    // throw Error("BF")
})

// Local.dispatch("Add",1)
// Local.dispatch("Add",2)
async function main(){
    await Local.dispatch({
        type:"Add",
        args:1
    });
    await Local.dispatch({
        type:"NULL",
        args:2
    });
    //清空对象的方法，需要被清空的对象，应只是本模块的局部/临时数据区，而不应该被export，否则设置为undefined的变量不会被GC回收。
    Local.Unlink();(<any>Local) = undefined; 
    console.log(Local)
}
main()
console.log("lalalal")

